Flask==1.1.2
Flask-Bcrypt==0.7.1
Flask-Cors==3.0.9
Flask-PyMongo==2.3.0
pymongo==3.11.0
pytest==6.1.1
jwt==1.0.0
PyJWT==1.4.2
