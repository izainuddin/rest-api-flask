#Rest API For User Management System using Python (Flask Micro framework)

- Python3 and Flask for the application
- MongoDb as a database
- Postman and pytest for testing
- Swagger openApi for documentation

#Description
- User can create new account
- User can login to their account using their email address and receive jwt token (valid for 30 mins)
- User can modify their names, password or email, they have to use a valid jwt token.
- Admin needs to login, receive jwt token and have user_id to delete the particular user.

#API documentation is available via openApi (swagger) at localhost:4200

Endpoints:
- /api/v1/users (Get(list the users, for admin only), Post (sign up))
- /api/v1/login (Post)
- /api/v1/users/{user_id}  (Patch, Delete)

# Build
sudo docker-compose build
sudo docker-compose up
