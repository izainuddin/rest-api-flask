# This module serves the endpoint /api/v1/usersEndpoint
from config import db
from app import app
from flask import jsonify, request, Response, render_template
from flask_bcrypt import Bcrypt
from bson.json_util import dumps
from bson.objectid import ObjectId
from bson.errors import InvalidId
import re, json, datetime, jwt
from constants import *
from functools import wraps

# import relevant db for this endpoint
users = db.users

bcrypt = Bcrypt(app)

app.config['SECRET_KEY']= 'thisismysecretkey'

def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if not token:
            return jsonify({'message' : 'Token is missing!'}), HTTP_UNAUTHORIZED

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = users.find_one({'email':data['user']})
            if not current_user: return jsonify({'message' : 'User not found, Token Invalid!'}), HTTP_UNAUTHORIZED
        except:
            return jsonify({'message' : 'Token is invalid!'}), HTTP_UNAUTHORIZED

        return f(current_user, *args, **kwargs)

    return decorated

# email validator
def valid_email(email):
  return bool(re.search(r"^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$", email))

# Error Processing
def _process_errors(data:tuple):
    with app.app_context():
        name, password, email = data
        ret = {}
        if  len(name) <3 :
            ret['name_error'] = "Name should contain at least 3 alphabet"
            #print(ret)
        if len(password) < 4:
            ret['password_error'] = "Password should contain at least 4 symbols"
            #print(ret)
        if not valid_email(email):
            ret['email_error'] = "Invalid format for email"
            #print(ret)
        if users.find_one({'email':email}):     # check for duplicate email
            ret['email_error'] = "Email already registered"
            #print(ret)

        status_code = HTTP_BAD_REQUEST
        if ret:
            return jsonify(ret), status_code

# Unprotected
# Captcha should be added here to protect from DoS attacks etc.
@app.route('/')
def get_root():
    print('sending docs')
    return render_template('swaggerui.html')

# CREATE A NEW USER (Unprotected)
# Captcha should be added here to protect from DoS attacks etc.
@app.route('/api/v1/users',methods=['POST'])
def add_user():
    ret = {}
    try:
        # new user details
        _name = request.json['name']
        _email = request.json['email']
        _password = request.json['password']
        error = _process_errors((_name, _password, _email))    # check if the entered data is good as per requirement
        if error: return error
        # encrpyt password if validated
        _hashed_password = bcrypt.generate_password_hash(_password)
        user = users.insert_one({'name':_name,'email':_email, 'password':_hashed_password})
        if not user:
            ret['error'] = 'error adding user, try again later'
            return  Response(json.dumps(ret,default=str),mimetype="application/json")
        ret['Status'] = 'User added successfully'
        added_user = users.find_one({'email':_email})
        ret['Unique ID'] = added_user['_id']
        return  Response(json.dumps(ret,default=str),mimetype="application/json"), HTTP_OK
    #except Exception as e:  return jsonify((e)), HTTP_INTERNAL_SERVER_ERROR
    except Exception as e:
        ret[str(e)] = ' Json does not contain required data'
        return Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_BAD_REQUEST

# User can login with email and password (Unprotected)
# Captcha should be added here to protect from DoS attacks etc.
@app.route('/api/v1/login',methods=['POST'])
def login():
    ret =  {}
    try:
        auth = request.authorization
        _email = auth["username"]
        _password = auth["password"]
        user = users.find_one({'email':_email})
        # if email not found
        if not user:
            ret['email_error'] = 'User not found'
            return Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_BAD_REQUEST
        # authenticate the password
        if not bcrypt.check_password_hash(user['password'],_password):
            ret['password_error'] = 'Authenication Failed'
            return Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_UNAUTHORIZED
        _date = datetime.datetime.now()
        users.update_one({'email':_email},{'$set':{'lastLogin':_date}})
        # create and send jwt token to the user when the login is successful
        token = jwt.encode({'user':_email, 'exp' : datetime.datetime.now() + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])
        ret['success'] = 'Logged in Successfully'
        ret['token'] = token.decode('UTF-8')
        #ret['token'] = token
        return Response(json.dumps(ret ,default=str),mimetype='application/json'), HTTP_OK
    except KeyError as e:
        ret[str(e)] = ' Json does not contain required data'
        return Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_BAD_REQUEST

# LIST all the users (Protected- Admin Only)
@app.route('/api/v1/users', methods=['GET'])
@token_required
def get_all_users(current_user):
    if current_user['email'] != 'admin':
        return jsonify({'message' : 'Access Denied, Admin Only API!'}), HTTP_UNAUTHORIZED
    try:
        list_of_users = users.find()
        list_of_users = [each_user for each_user in list_of_users]
        return  Response(json.dumps(list_of_users,default=str),mimetype="application/json"), HTTP_OK
    except InvalidId as e: return jsonify(str(e)), HTTP_BAD_REQUEST

# UPDATE an existing user (Protected for each user)
@app.route('/api/v1/users/<user_id>',methods=['PATCH'])
@token_required
def update_user(current_user,user_id):
    ret = {}
    try:
        # Check if the user id is valid
        user = users.find_one({'_id':ObjectId(user_id)})
        if not users.find_one({'_id':ObjectId(user_id)}):
            ret['user_error'] = 'No User Found with this ID'
            return  Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_BAD_REQUEST

        # check if email matches the object ID, else HTTP_UNAUTHORIZED
        #if (current_user['email'] != user['email']) and (current_user['email'] != 'admin'):
        if (current_user['email'] != user['email']):
            ret['email_error'] = 'Authenication Failed, Login with your own email first'
            return Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_UNAUTHORIZED
        try:
            # check which fields are needed to be updated
            to_update = request.get_json(force=True)

            # updated values need to be checked as per requirements, name email and password
            for key in to_update:
                val = to_update[key]
                if key=='name':
                    if  len(val) <3 :
                        ret['name_error'] = "Name should contain at least 3 alphabets"

                if key=='email':
                    if not valid_email(val):
                        ret['email_error'] = "Invalid format for email"
                    else:
                        try:
                            find_email = users.find_one({'email':val})
                            if (str(find_email['_id']) != user_id):     # check for duplicate email other than this users own email
                                ret['email_error'] = "Email already in use"
                        except: pass                    # if no email was found, then PASS

                if key=='password':
                    if len(val) < 4:
                        ret['password_error'] = "Password should contain at least 4 symbols"
                    else:                                              # hash the Password
                        to_update[key] = bcrypt.generate_password_hash(val)

            # if there is any error then return with error names
            if ret:     return Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_BAD_REQUEST

            # if not error found update user
            users.update_one({'_id' : ObjectId(user_id)},{'$set': to_update})
            return Response(json.dumps('User Updated Successfully',default=str),mimetype='application/json'), HTTP_OK
        except:
            ret['error'] = 'Nothing to update, Please specify updated name, email and/or password'
            return Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_BAD_REQUEST
    except InvalidId as e: return jsonify(str(e)), HTTP_BAD_REQUEST


# Delete a user with their unique ID (Protected: Admin Only)
@app.route('/api/v1/users/<user_id>',methods=['DELETE'])
@token_required
def delete_user(current_user,user_id):
    ret = {}
    try:
        if current_user['email'] != 'admin':
            return jsonify({'message' : 'Access Denied, Admin Only API!'}), HTTP_UNAUTHORIZED
        # Check if the user id is valid
        if not users.find_one({'_id':ObjectId(user_id)}):
            ret['user_error'] = 'No User Found with this ID'
            return  Response(json.dumps(ret,default=str),mimetype='application/json'), HTTP_BAD_REQUEST
        # deleted the user. May be take password from the user before they delete it
        users.delete_one({'_id':ObjectId(user_id)})
        ret['success'] = 'User deleted successfully'
        return  Response(json.dumps(ret,default=str),mimetype="application/json")
    except InvalidId as e: return jsonify(str(e)), HTTP_BAD_REQUEST
