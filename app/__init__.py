# Init module

from flask import Flask

app = Flask(__name__)
app.debug = True

# import users' endpoint
from app import usersEndpoint
