FROM python:latest
RUN apt update & apt -y upgrade
ENV PYTHONUNBUFFERED=1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
EXPOSE 4200
CMD [ "python", "./run-app.py"]
